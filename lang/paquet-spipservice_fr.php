<?php

/*______________________________________________________________________________
 | Plugin SpipService 1.0 pour Spip 3                                           \
 | Copyright 2012 Sebastien Chandonay - Studio Lambda                            \
 |                                                                                |
 | SpipService est un logiciel libre : vous pouvez le redistribuer ou le          |
 | modifier selon les termes de la GNU General Public Licence tels que            |
 | publi�s par la Free Software Foundation : � votre choix, soit la               |
 | version 3 de la licence, soit une version ult�rieure quelle qu'elle            |
 | soit.                                                                          |
 |                                                                                |
 | SpipService est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE     |
 | GARANTIE ; sans m�me la garantie implicite de QUALIT� MARCHANDE ou             |
 | D'AD�QUATION � UNE UTILISATION PARTICULI�RE. Pour plus de d�tails,             |
 | reportez-vous � la GNU General Public License.                                 |
 |                                                                                |
 | Vous devez avoir re�u une copie de la GNU General Public License               |
 | avec SpipService. Si ce n'est pas le cas, consultez                            |
 | <http://www.gnu.org/licenses/>                                                 |
 ________________________________________________________________________________*/

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
		'spipservice_nom' => "SpipService",
		'spipservice_slogan' => "Actualisez votre site depuis des applications tierces",
		'spipservice_description' => "Permet à une application tierce de consulter ou modifier le contenu de votre site (rubrique/article/breve/document). Cette API webservice est basée sur le protocole HTTP REST",
);

?>